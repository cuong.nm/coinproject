﻿using System.Web.Mvc;

namespace CRYPTO.UI.Areas.Hire_Me
{
    public class Hire_MeAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Hire_Me";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute(
            //    "Hire_Me_default",
            //    "Hire_Me/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}