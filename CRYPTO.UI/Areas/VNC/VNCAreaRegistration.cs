﻿using System.Web.Mvc;

namespace CRYPTO.UI.Areas.VNC
{
    public class VNCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "VNC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            //context.MapRoute(
            //    "VNC_default",
            //    "VNC/{controller}/{action}/{id}",
            //    new { action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}