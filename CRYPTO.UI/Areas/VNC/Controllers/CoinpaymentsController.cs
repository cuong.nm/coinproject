﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CRYPTO.UI.Areas.VNC.Controllers
{
    [RouteArea("VNC", AreaPrefix = "vnc")]
    [Route("{action}")]
    public class CoinpaymentsController : Controller
    {
        private static string merchant_id = "83f962bfb0c6a8acd0228d7bfb521c2c";
        private static string secret = "B1E8B4692B7E2D898179740F86F11D5B85B1C9F3CE468E74B90237FE93D0F439660975FB02C07B3CD40BCF0850666C66F75789034429F95263929AB13C6387B4";
        private static string SECRET_HASH = "B1E8B4692B7E2D898179740F86F11D5B85B1C9F3CE468E74B90237FE93D0F439660975FB02C07B3CD40BCF0850666C66F75789034429F95263929AB13C6387B4";
        private static string SECRET_HASH_WITHDRAW = "B1E8B4692B7E2D898179740F86F11D5B85B1C9F3CE468E74B90237FE93D0F439660975FB02C07B3CD40BCF0850666C66F75789034429F95263929AB13C6387B4";
        private static readonly Encoding encoding = Encoding.UTF8;

        // GET: Hire_Me/Coinpayments
        protected string StartUpPath
        {
            get
            {
                try
                {
                    return System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Logs/vnc");
                }
                catch (Exception)
                {
                    return "/Logs/vnc";
                }
            }
        }

        [Route("get-list-wallet")]
        [HttpGet]
        public ActionResult GetListWallet(string userCode = "")
        {
            try
            {
                if (userCode == "")
                    return Json(new { status = 0, data = new { }, msg = "UserCode: Invaid" }, JsonRequestBehavior.AllowGet);

                List<string> coins = new List<string>();
                coins.Add("BTC");
                coins.Add("ETH");
                coins.Add("USDT.ERC20");

                List<UserCoins> lstUserCoins = new List<UserCoins>();
                if (coins != null && coins.Count() > 0)
                {
                    foreach (var item in coins)
                    {
                        var parms = new SortedList<string, string>();
                        parms["ipn_url"] = "http://crypto.sapp.asia/vnc/ipn-url";
                        parms["label"] = "VNC_" + userCode;
                        var result = libCoinPayments_VNC.GetDepositAddress(EnumCmdCoinpayments.CALL_BACK_ADDRESS, item, parms);
                        string address = result["result"]["address"];
                        string memo = "";
                        UserCoins userCoins = new UserCoins();
                        userCoins.Code = item;
                        userCoins.Address = address;
                        userCoins.Memo = memo;
                        lstUserCoins.Add(userCoins);
                    }
                }

                var obj = from a in lstUserCoins
                          select new
                          {
                              code = a.Code,
                              address = a.Address,
                              memo = a.Memo,
                          };
                return Json(new { status = 1, data = obj, msg = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, data = new { }, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        [Route("get-transaction")]
        [HttpGet]
        public ActionResult GetTransaction()
        {
            var parms = new SortedList<string, string>();
            parms["all"] = "1";
            var obj = libCoinPayments_VNC.CallAPI(EnumCmdCoinpayments.TRANSACTION, parms);
            return Json(obj, JsonRequestBehavior.AllowGet);

        }

        [Route("get-info")]
        [HttpGet]
        public ActionResult GetInfo(string cmd, string id)
        {
            var parms = new SortedList<string, string>();
            parms["id"] = id;
            parms["txid"] = id;
            var obj = libCoinPayments_VNC.CallAPI(cmd, parms);
            return Json(obj, JsonRequestBehavior.AllowGet);

        }

        [Route("ipn-url")]
        [HttpPost]
        public ActionResult IPN(string ipn_version = "", string ipn_type = "", string ipn_mode = "", string ipn_id = "", string merchant = "", string txn_id = "", string address = "", string dest_tag = "", int? status = null, string status_text = "", string currency = "", int? confirms = null, double? amount = null, double? amounti = null, double? fee = null, double? feei = null, string fiat_coin = "", double? fiat_amount = null, double? fiat_amounti = null, double? fiat_fee = null, double? fiat_feei = null, string id = "")
        {
            try
            {
                var obj = new
                {
                    ipn_version = ipn_version,
                    ipn_type = ipn_type,
                    ipn_mode = ipn_mode,
                    ipn_id = ipn_id,
                    merchant = merchant,
                    txn_id = txn_id,
                    address = address,
                    dest_tag = dest_tag,
                    status = status,
                    status_text = status_text,
                    currency = currency,
                    confirms = confirms,
                    amount = amount,
                    amounti = amounti,
                    fee = fee,
                    feei = feei,
                    fiat_coin = fiat_coin,
                    fiat_amount = fiat_amount,
                    fiat_amounti = fiat_amounti,
                    fiat_fee = fiat_fee,
                    fiat_feei = fiat_feei,
                };

                var http_hmac = Request["HTTP_HMAC"];

                var serializer = new JavaScriptSerializer();
                string json = serializer.Serialize(obj);

                PublishFunctions.WriteLog(StartUpPath, "input", obj.ToString(), "checkSUm :" + http_hmac, "IP address : " + IpAddress);

                if (string.IsNullOrEmpty(http_hmac))
                    return Json(new { status = 0, msg = "Errors" }, JsonRequestBehavior.AllowGet);

                if (string.IsNullOrEmpty(merchant))
                    return Json(new { status = 0, msg = "Errors" }, JsonRequestBehavior.AllowGet);

                if (merchant != merchant_id)
                    return Json(new { status = 0, msg = "Errors" }, JsonRequestBehavior.AllowGet);

                //process IPN here
                if (ipn_type == "deposit" && status >= 100)
                {
                    int type = currency == "BTC" ? 1 : (currency == "ETH" ? 2 : 4);
                    var tx = txn_id;
                    // hash
                    string str = SECRET_HASH + type + "&" + (amount - fee) + "&" + address + "&" + tx;
                    string secure_hash_check = PublishFunctions.ComputeSha256Hash(PublishFunctions.GetMd5Hash2(PublishFunctions.GetMd5Hash2(str)));

                    //pre data
                    var parms = new SortedList<string, string>();
                    parms["type"] = type.ToString();
                    parms["amount"] = (amount - fee).ToString();
                    parms["fee"] = fee.ToString();
                    parms["address"] = address;
                    if (type == EnumCoinType.BNB)
                        parms["memo"] = dest_tag;
                    parms["tx"] = txn_id;
                    parms["secure_hash"] = secure_hash_check;

                    string post_data = "";
                    foreach (KeyValuePair<string, string> parm in parms)
                    {
                        if (post_data.Length > 0) { post_data += "&"; }
                        post_data += parm.Key + "=" + Uri.EscapeDataString(parm.Value);
                    }
                    // do the post:
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    System.Net.WebClient cl = new System.Net.WebClient();
                    cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    cl.Encoding = encoding;

                    var ret = new Dictionary<string, dynamic>();
                    try
                    {
                        string resp = cl.UploadString("http://vnc.sapp.asia/api/deposit", post_data);
                        var decoder = new System.Web.Script.Serialization.JavaScriptSerializer();
                        ret = decoder.Deserialize<Dictionary<string, dynamic>>(resp);
                    }
                    catch (System.Net.WebException e)
                    {
                        ret["error"] = "Exception while contacting CoinPayments.net: " + e.Message;
                    }
                    catch (Exception e)
                    {
                        ret["error"] = "Unknown exception: " + e.Message;
                    }

                    PublishFunctions.WriteLog(StartUpPath, "input", serializer.Serialize(ret).ToString(), "checkSUm :" + http_hmac, "IP address : " + IpAddress);
                }
                else if (ipn_type == "withdrawal")
                {
                    var status_withdrawal = status_text == "Complete" ? 1 : 2;
                    var transaction_hash = txn_id;
                    string str = SECRET_HASH_WITHDRAW + status_withdrawal + "&" + transaction_hash;
                    string secure_hash_check = PublishFunctions.ComputeSha256Hash(PublishFunctions.GetMd5Hash2(PublishFunctions.GetMd5Hash2(str)));
                    //pre data
                    var parms = new SortedList<string, string>();
                    parms["status"] = status_withdrawal.ToString();
                    parms["transaction_hash"] = transaction_hash;
                    parms["secure_hash"] = secure_hash_check;
                    parms["id"] = id;

                    string post_data = "";
                    foreach (KeyValuePair<string, string> parm in parms)
                    {
                        if (post_data.Length > 0) { post_data += "&"; }
                        post_data += parm.Key + "=" + Uri.EscapeDataString(parm.Value);
                    }
                    // do the post:
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    System.Net.WebClient cl = new System.Net.WebClient();
                    cl.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                    cl.Encoding = encoding;

                    var ret = new Dictionary<string, dynamic>();
                    try
                    {
                        string resp = cl.UploadString("http://vnc.sapp.asia/api/withDrawlConfirm", post_data);
                        var decoder = new System.Web.Script.Serialization.JavaScriptSerializer();
                        ret = decoder.Deserialize<Dictionary<string, dynamic>>(resp);
                    }
                    catch (System.Net.WebException e)
                    {
                        ret["error"] = "Exception while contacting CoinPayments.net: " + e.Message;
                    }
                    catch (Exception e)
                    {
                        ret["error"] = "Unknown exception: " + e.Message;
                    }

                    PublishFunctions.WriteLog(StartUpPath, "input", serializer.Serialize(ret).ToString(), "checkSUm :" + http_hmac, "IP address : " + IpAddress);
                }
                return Json(new { status = 1, data = obj, msg = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 0, data = new { }, msg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("withdrawal")]
        [HttpPost]
        public ActionResult Withdrawal(double? amount = null, string sender = "", string recipient = "", int? type = null, string secure_hash = "")
        {
            try
            {
                var obj = new
                {
                    amount = amount,
                    sender = sender,
                    recipient = recipient,
                    type = type,
                    secure_hash = secure_hash,
                };

                var serializer = new JavaScriptSerializer();
                string json = serializer.Serialize(obj);
                PublishFunctions.WriteLog(StartUpPath, "input", json, "checkSUm :" + secure_hash, "IP address : " + IpAddress);

                string str = SECRET_HASH_WITHDRAW + type + "&" + amount + "&" + recipient;
                string secure_hash_check = PublishFunctions.ComputeSha256Hash(PublishFunctions.GetMd5Hash2(PublishFunctions.GetMd5Hash2(str)));
                if (!secure_hash_check.Equals(secure_hash))
                    return Json(new { success = false, message = "", errors = new { msg = "checksum is wrong!" } }, JsonRequestBehavior.AllowGet);

                var parms = new SortedList<string, string>();
                parms["amount"] = amount.ToString();
                parms["currency"] = EnumCoinTypeVNC.ToString(type);
                parms["address"] = recipient;
                parms["auto_confirm"] = "1";
                parms["ipn_url"] = "http://crypto.sapp.asia/vnc/ipn-url";
                var result = libCoinPayments_VNC.Withdrawal(EnumCmdCoinpayments.WITHDRAWAL, parms);

                PublishFunctions.WriteLog(StartUpPath, "result", result.ToString(), "checkSUm :" + secure_hash, "IP address : " + IpAddress);

                if (result["error"] == "ok")
                {
                    var data = new
                    {
                        status = "pending",
                        transaction_hash = result["result"]["id"],
                        sender = sender,
                        recipient = recipient,
                        amount = result["result"]["amount"],
                        coin = EnumCoinTypeVNC.ToString(type),
                    };
                    return Json(new { success = true, data = data }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false, data = new { } }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = false, message = "", errors = new { msg = ex.Message } }, JsonRequestBehavior.AllowGet);
            }
        }


        [Route("create-merchant")]
        public ActionResult CreateMerchant()
        {
            Encoding encoding = Encoding.UTF8;
            var key = "fojVuiJgEnaSMdJwO6lywADaH5XxI8/xW3xSGQbtg55LSrHekF4N/R0GXsg7mATYm5jwRpuwM7X4SuOiiTeqKcMnrWwvoLojIUHJSd6oTkLVE+JlvbR7a3SJuCjZzNvkmPmP8TTmGXxVr6rclCWWfpIDOtE94I7Y48u17aOjyeEmWrzpJjSEjg==";
            byte[] keyBytes = encoding.GetBytes(key);
            var hmacsha512 = new System.Security.Cryptography.HMACSHA512(keyBytes);
            string hmac = BitConverter.ToString(hmacsha512.ComputeHash(keyBytes)).Replace("-", string.Empty);

            return Json(hmac, JsonRequestBehavior.AllowGet);

        }

        protected string IpAddress
        {
            get
            {
                string visitorIPAddress = "";
                try
                {
                    bool GetLan = false;
                    visitorIPAddress = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                    if (string.IsNullOrEmpty(visitorIPAddress))
                        visitorIPAddress = Request.ServerVariables["REMOTE_ADDR"];

                    if (string.IsNullOrEmpty(visitorIPAddress))
                        visitorIPAddress = Request.UserHostAddress;

                    if (string.IsNullOrEmpty(visitorIPAddress) || visitorIPAddress.Trim() == "::1")
                    {
                        GetLan = true;
                        visitorIPAddress = string.Empty;
                    }

                    if (GetLan)
                    {
                        if (string.IsNullOrEmpty(visitorIPAddress))
                        {
                            //This is for Local(LAN) Connected ID Address
                            string stringHostName = Dns.GetHostName();
                            //Get Ip Host Entry
                            IPHostEntry ipHostEntries = Dns.GetHostEntry(stringHostName);
                            //Get Ip Address From The Ip Host Entry Address List
                            IPAddress[] arrIpAddress = ipHostEntries.AddressList;

                            try
                            {
                                visitorIPAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                            }
                            catch
                            {
                                try
                                {
                                    visitorIPAddress = arrIpAddress[0].ToString();
                                }
                                catch
                                {
                                    try
                                    {
                                        arrIpAddress = Dns.GetHostAddresses(stringHostName);
                                        visitorIPAddress = arrIpAddress[0].ToString();
                                    }
                                    catch
                                    {
                                        visitorIPAddress = string.Empty;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                return visitorIPAddress;
            }
        }
    }
    public class UserCoins
    {
        public string Code { get; set; }
        public string Address { get; set; }
        public string Memo { get; set; }
    }
}