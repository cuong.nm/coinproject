﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRYPTO.UI
{
    public class EnumCmdCoinpayments
    {
        public const string RATES = "rates";
        public const string DEPOSIT = "get_deposit_address";
        public const string CALL_BACK_ADDRESS = "get_callback_address";
        public const string BALANCE = "balances";
        public const string TRANSACTION = "get_tx_ids";
        public const string WITHDRAWAL = "create_withdrawal";
        public const string GETWITHDRAWALINFO = "get_withdrawal_info";

    }


    public class EnumUserStatus
    {
        public const int ACTIVE = 1;
        public const int INACTIVE = 2;
        public const int BLOCK = 3;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case ACTIVE:
                    return "Đang hoạt động";
                case INACTIVE:
                    return "Không hoạt động";
                case BLOCK:
                    return "Block";
                default:
                    return "";
            }
        }
    }

    public class EnumPermission
    {
        public const int SUPER_ADMIN = 0;
        public const int ADMIN = 1;
        public const int EMPLOYEES = 2;
        public const int MEMBER = 3;
        public const int CUSTOMER = 4;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case SUPER_ADMIN:
                    return "Quản trị viên master";
                case ADMIN:
                    return "Quản trị viên";
                case EMPLOYEES:
                    return "Nhân viên";
                case MEMBER:
                    return "Thành viên";
                case CUSTOMER:
                    return "Khách hàng";
                default:
                    return "";
            }
        }
    }

    public class EnumYesNo
    {
        public const int YES = 1;
        public const int NO = 0;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case YES:
                    return "Có";
                case NO:
                    return "Không";
                default:
                    return "";
            }
        }
    }

    public class EnumGender
    {
        public const int MALE = 1;
        public const int FEMALE = 2;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case MALE:
                    return "Nam";
                case FEMALE:
                    return "Nữ";
                default:
                    return "";
            }
        }
    }


    public class EnumStatus
    {
        public const int ALL = 0;
        public const int ACTIVE = 1;
        public const int INACTIVE = 2;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case ALL:
                    return "Tất cả";
                case ACTIVE:
                    return "Kích hoạt";
                case INACTIVE:
                    return "Chưa kích hoạt";
                default:
                    return "";
            }
        }
    }
    public class EnumGroupType
    {
        public const int GROUP_MESSAGE = 1;
        public const int PRIVATE_MESSAGE = 2;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case GROUP_MESSAGE:
                    return "Chat nhóm";
                case PRIVATE_MESSAGE:
                    return "Chat cá nhân";
                default:
                    return "";
            }
        }
    }

    public class EnumMessageType
    {
        public const int MESSAGE = 1;
        public const int VIDEO = 2;
        public const int IMAGE = 3;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case MESSAGE:
                    return "Tin nhắn";
                case VIDEO:
                    return "Video";
                case IMAGE:
                    return "Hình ảnh";
                default:
                    return "";
            }
        }
    }

    public class EnumNotificationType
    {
        public const int NEW_MESSAGE = 1; //add
        public const int DELETE_MESSAGE = 2;
        public const int ADD_GROUP = 3;
        public const int FRIEND_REQUEST = 4; //add
        public const int TRANSFER = 5; //add
        public const int NOTE = 6; //add
        public const int TIPS = 7; //add
        public const int BOOKING_SUCCESS_PG = 8; //add
        public const int BOOKING_SUCCESS_CUSTOMER = 9; //add
        public const int BOOKING_CANCEL_PG = 10; //add
        public const int BOOKING_CANCEL_CUSTOMER = 11; //add
        public const int NEWS_NOTIFICATION = 12; //add
        public const int REFERRAL = 13; //add
        public const int CONFIRM_REFERRAL = 14; //add

        public static string ToString(int? value)
        {
            switch (value)
            {
                case NEW_MESSAGE:
                    return "Tin nhắn mới";
                case DELETE_MESSAGE:
                    return "Xóa tin nhắn";
                case ADD_GROUP:
                    return "Thêm mới nhóm";
                case FRIEND_REQUEST:
                    return "Yêu cầu kết bạn";
                case TRANSFER:
                    return "Chuyển tiền";
                case NOTE:
                    return "Nhắc nhở";
                case TIPS:
                    return "Tips";
                case BOOKING_SUCCESS_PG:
                    return "Booking thành công tới PG";
                case BOOKING_SUCCESS_CUSTOMER:
                    return "Booking thành công tới khách hàng";
                case BOOKING_CANCEL_PG:
                    return "Hủy booking tới PG";
                case BOOKING_CANCEL_CUSTOMER:
                    return "Hủy booking tới khách hàng";
                case NEWS_NOTIFICATION:
                    return "Thông báo mới";
                case REFERRAL:
                    return "Giới thiệu người dùng";
                case CONFIRM_REFERRAL:
                    return "Xác nhận giới thiệu";
                default:
                    return "";
            }
        }
    }
    public class EnumFriendStatus
    {
        public const int FRIEND_REQUEST = 1;
        public const int FRIEND = 2;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case FRIEND_REQUEST:
                    return "Yêu cầu kết bạn";
                case FRIEND:
                    return "Bạn bè";
                default:
                    return "";
            }
        }
    }
   
    public class EnumTransactionType
    {
        public const int RECHARGE = 1;
        public const int TRANSFER = 2;
        public const int BUY_CARD = 3;
        public const int BOOKING = 4;
        public const int REFERRAL = 5;
        public const int WITH_DRAWAL = 6;
        public const int TIPS = 7;
        public const int PROFIT_SYSTEM = 8;
        public const int EXCHANGE_GIFTS = 9;
        public const int BUY_VIP = 10;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case RECHARGE:
                    return "Nạp tiền";
                case TRANSFER:
                    return "Chuyển tiền";
                case BUY_CARD:
                    return "Mua sim thẻ";
                case BOOKING:
                    return "Thanh toán";
                case REFERRAL:
                    return "Giới thiệu";
                case WITH_DRAWAL:
                    return "Rút tiền";                                             
                case TIPS:
                    return "Tips";
                case PROFIT_SYSTEM:
                    return "Hoa hồng hệ thống";
                case EXCHANGE_GIFTS:
                    return "Đổi quà";
                case BUY_VIP:
                    return "Mua vip";
                default:
                    return "";
            }
        }
    }

    public class EnumOrderBy
    {
        public const int NAME_ASC = 1;
        public const int NAME_DESC = 2;
        public const int DATE_DESC = 3;
        public const int DATE_ASC = 4;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case NAME_ASC:
                    return "A==>Z";
                case NAME_DESC:
                    return "Z==>A";
                case DATE_DESC:
                    return "Mới nhất";
                case DATE_ASC:
                    return "Cũ nhất";
                default:
                    return "";
            }
        }
    }

    public class EnumIdentificationType
    {
        public const int PEOPLE_ID = 1;
        public const int ID_CARD = 2;
        public const int PASSPORT = 3;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case PEOPLE_ID:
                    return "Chứng minh thư nhân dân";
                case ID_CARD:
                    return "Thẻ căn cước công dân";
                case PASSPORT:
                    return "Hộ chiếu";
                default:
                    return "";
            }
        }
    }

    public class EnumSearchType
    {
        public const int NEWS = 1;
        public const int SERVICE = 2;
        public const int PG = 3;
        public const int LOCATION = 4;
        public const int COUPON = 5;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case NEWS:
                    return "Tin tức";
                case SERVICE:
                    return "Dịch vụ";
                case PG:
                    return "PG";
                case LOCATION:
                    return "Địa điểm";
                case COUPON:
                    return "Khuyến mãi";
                default:
                    return "";
            }
        }
    }
    public class EnumDiscountType
    {
        public const int PERCENT = 1;
        public const int CASH = 2;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case PERCENT:
                    return "Phần trăm";
                case CASH:
                    return "Tiền mặt";
                default:
                    return "";
            }
        }
    }
    public class EnumSeverity
    {
        public const int LEVEL1 = 1;
        public const int LEVEL2 = 2;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case LEVEL1:
                    return "Level 1";
                case LEVEL2:
                    return "Level 2";
                default:
                    return "";
            }
        }
    }
    public class EnumInvoiceStatus
    {
        public const int ALL = 0;
        public const int WAIT_FOR_PAY = 1;
        public const int WAIT_FOR_CONFIRMATION = 2;
        public const int RECEIVED = 3;
        public const int COMING = 4;
        public const int DONE = 5;
        public const int CANCEL = 6;
        public const int START = 7;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case ALL:
                    return "Tất cả";
                case WAIT_FOR_PAY:
                    return "Chưa thanh toán";
                case WAIT_FOR_CONFIRMATION:
                    return "Chờ xác nhận";
                case RECEIVED:
                    return "Đã nhận";
                case COMING:
                    return "Đang đến";
                case DONE:
                    return "Đã xong";
                case CANCEL:
                    return "Đã hủy";
                case START:
                    return "Đang diễn ra";
                default:
                    return "";
            }
        }
    }

    public class EnumPaymentMethod
    {
        public const int PAYPAL = 1;
        public const int VISA = 2;
        public const int ATM = 3;
        public const int COIN = 4;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case PAYPAL:
                    return "Thanh toán qua Paypal";
                case VISA:
                    return "Thẻ quốc tế Visa, Master Card";
                case ATM:
                    return "Thẻ ATM nội địa/Internet banking";
                case COIN:
                    return "Thanh toán bằng coin";
                default:
                    return "";
            }
        }
    }
    
    public class EnumPaymentStatus
    {
        public const int PENDING = 1;
        public const int SUCCESS = 2;
        public const int ERROR = 3;

        public static string ToString(int? value)
        {
            switch (value)
            {
                case PENDING:
                    return "Chờ thanh toán";
                case SUCCESS:
                    return "Thành công";
                case ERROR:
                    return "Lỗi";
                default:
                    return "";
            }
        }
    }

    public class EnumCoinType
    {
        public const int BTC = 1;
        public const int ETH = 2;
        public const int LTC = 3;
        public const int USDT = 4;
        public const int CP = 5;
        public const int BNB = 6;
        public const int DOGE = 7;
        public const int ADA = 8;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case BTC:
                    return "BTC";
                case ETH:
                    return "ETH";
                case LTC:
                    return "LTC";
                case USDT:
                    return "USDT";
                case CP:
                    return "CP";
                case BNB:
                    return "BNB";
                case DOGE:
                    return "DOGE";
                case ADA:
                    return "ADA";
                default:
                    return "";
            }
        }
    }

    public class EnumCoinTypeVNC
    {
        public const int BTC = 1;
        public const int ETH = 2;
        public const int USDT = 4;
        public const int VNC = 5;
        public static string ToString(int? value)
        {
            switch (value)
            {
                case BTC:
                    return "BTC";
                case ETH:
                    return "ETH";
                case USDT:
                    return "USDT.ERC20";
                case VNC:
                    return "VNC";
                default:
                    return "";
            }
        }
    }
}