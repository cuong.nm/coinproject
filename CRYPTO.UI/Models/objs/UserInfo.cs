﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CRYPTO.MODEL;

namespace CRYPTO.UI
{
    public class LocationNow
    {
        public string latitude { get; set; }
        public string longitude { get; set; }        
    }
}