﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using CRYPTO.MODEL;
using Newtonsoft.Json;

namespace CRYPTO.UI
{
    public static class PublishFunctions
    {
        public static bool CheckTonTai(string id, string list1)
        {
            if (list1 == null || list1.Trim() == "" || list1.Split(',').Count() == 0 || id == null)
                return false;
            List<string> lst1 = list1.Split(',').ToList();
            foreach (var item in lst1)
            {
                if (item == id)
                    return true;
            }
            return false;
        }
        public static int CheckPhanTu(string list1, string list2)
        {
            if (list1 == null || list1.Trim() == "" || list1.Split(',').Count() == 0 || list2 == null || list2.Trim() == "" || list2.Split(',').Count() == 0)
                return 0;

            var set = new HashSet<int>(list1.Select(x => int.Parse(x.ToString())));
            var equals = set.SetEquals(list2.Select(x => int.Parse(x.ToString())));
            if (equals)
                return 1;
            return 0;
        }
        public static int CheckHaiMang(string list1, string list2)
        {
            if (list1 == null || list1.Trim() == "" || list1.Split(',').Count() == 0 || list2 == null || list2.Trim() == "" || list2.Split(',').Count() == 0)
                return 0;

            List<string> lst1 = list1.Split(',').ToList(), lst2 = list2.Split(',').ToList();
            foreach (var item in lst1)
            {
                if (lst2.Count(x => x == item) > 0)
                    return 1;
            }
            return 0;
        }
        public static string CreateCode()
        {
            var str = "";
            //IPMEntities db = new IPMEntities();
            //var appConfig = db.CauHinhs.FirstOrDefault();
            //var str = Guid.NewGuid().ToString("n").Substring(0, appConfig.CodeLength.Value).ToUpper();
            //var check = db.NguoiDungs.FirstOrDefault(x => x.Code == str);
            //if (check != null)
            //{
            //    PublishFunctions.CreateCode();
            //}
            return str;
        }
        public static string ToFormat12h(this DateTime dt)
        {
            return dt.ToString("yyyy/MM/dd hh:mm:ss tt");
        }

        public static string ToFormat24h(this DateTime dt)
        {
            return dt.ToString("yyyy/MM/dd HH:mm:ss");
        }

        public static bool CheckExtensionImage(string ext)
        {
            string list = "'aai', 'art', 'arw', 'avs', 'bmp', 'bpg', 'brf', 'cals', 'cgm', 'cr2', 'crw', 'cur', 'cut', 'dcm', 'dcr', 'dcx', 'dib', 'dng', 'emf', 'fax', 'gif', 'hdr', 'hrz', 'ico', 'info', 'isobrl', 'isobrl6', 'jng', 'jpeg', 'mat', 'mng', 'mono', 'mpc', 'mrw', 'mtv', 'mvg', 'nef', 'orf', 'otb', 'p7', 'palm', 'pam', 'pbm', 'pcx', 'pes', 'pfa', 'pfb', 'pfm', 'pgm', 'picon', 'pict', 'pix', 'png', 'pnm', 'ppm', 'psb', 'psd', 'ptif', 'pwp', 'rad', 'raf', 'rgf', 'rla', 'rle', 'sct', 'sfw', 'sgi', 'sun', 'svg', 'tga', 'tiff', 'tim', 'ttf', 'ubrl', 'ubrl6', 'uil', 'uyvy', 'vacar', 'viff', 'wbpm', 'wpg', 'x3f', 'xbm', 'xcf', 'xmp', 'xwd', 'dxf', 'eps', 'pcd', 'pct', 'wmf', 'jpg', 'ai', 'tif', 'dst', 'dds', 'dwg', 'raw', 'webp', 'cdr', 'heic', 'dfont', 'ps', 'nrw', 'plt','AAI', 'ART', 'ARW', 'AVS', 'BMP', 'BPG', 'BRF', 'CALS', 'CGM', 'CR2', 'CRW', 'CUR', 'CUT', 'DCM', 'DCR', 'DCX', 'DIB', 'DNG', 'EMF', 'FAX', 'GIF', 'HDR', 'HRZ', 'ICO', 'INFO', 'ISOBRL', 'ISOBRL6', 'JNG', 'JPEG', 'MAT', 'MNG', 'MONO', 'MPC', 'MRW', 'MTV', 'MVG', 'NEF', 'ORF', 'OTB', 'P7', 'PALM', 'PAM', 'PBM', 'PCX', 'PES', 'PFA', 'PFB', 'PFM', 'PGM', 'PICON', 'PICT', 'PIX', 'PNG', 'PNM', 'PPM', 'PSB', 'PSD', 'PTIF', 'PWP', 'RAD', 'RAF', 'RGF', 'RLA', 'RLE', 'SCT', 'SFW', 'SGI', 'SUN', 'SVG', 'TGA', 'TIFF', 'TIM', 'TTF', 'UBRL', 'UBRL6', 'UIL', 'UYVY', 'VACAR', 'VIFF', 'WBPM', 'WPG', 'X3F', 'XBM', 'XCF', 'XMP', 'XWD', 'DXF', 'EPS', 'PCD', 'PCT', 'WMF', 'JPG', 'AI', 'TIF', 'DST', 'DDS', 'DWG', 'RAW', 'WEBP', 'CDR', 'HEIC', 'DFONT', 'PS', 'NRW', 'PLT'";
            ext = ext.Replace(".", "");
            if (list.Contains(ext))
                return true;
            else
                return false;
        }

        public static string GetMd5Hash2(string input)
        {
            byte[] data = MD5.Create().ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));
                StringBuilder builder = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    builder.Append(bytes[i].ToString("x2"));
                }
                return builder.ToString();
            }
        }

        public static void WriteLog(string pathRoot, params string[] content)
        {
            try
            {
                DateTime dt = DateTime.Now;

                string directoryPath = Path.Combine(pathRoot, dt.Year.ToString(), dt.Month.ToString());
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }

                string filePath = Path.Combine(directoryPath, "Log_" + dt.ToString("yyyyMMdd") + ".txt");
                StreamWriter sw;
                if (!File.Exists(filePath))
                {
                    sw = File.CreateText(filePath);
                }
                else
                {
                    sw = File.AppendText(filePath);
                    sw.WriteLine();
                }

                sw.WriteLine(dt.ToString("HH:mm:ss"));
                foreach (string item in content)
                {
                    sw.WriteLine(item);
                }
                sw.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}